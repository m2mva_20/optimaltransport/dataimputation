#!/bin/python


import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import sklearn.datasets as sk_data
from tqdm import tqdm


def nans(shape, dtype=float):
    a = np.empty(shape, dtype)
    a.fill(np.nan)
    return a

def plotData(X):
    plt.figure()
    plt.plot(X[:,0,0], X[:,1,0], 'o')
    plt.show()

def genData(n=100, d=2, missing_ratio=.2):
    X_all        = rd.normal(size=(n,d,1))
    binary_mask  = rd.choice([0,1], size=(n,d,1), p=[missing_ratio,1-missing_ratio])
    X_obs        = X_all * binary_mask
    return X_all, X_obs, binary_mask

def sampler(T, batch_size,n_batch=2):
    n,d,a   = T.shape
    indexes = list(range(n))
    batches = [np.zeros((batch_size,d,a)) for i in range(n_batch)]
    for n in range(n_batch):
        for i in range(batch_size):
            j = rd.choice(indexes)
            indexes.remove(j)
            batches[n][i,:,:] = T[j,:,:]
    return batches

def sinkhorn(K, a, b, n_iter=2000):
    v = np.ones(K.shape[0])
    for i in range(n_iter):
        u = a / (K*v).sum(axis=1)
        v = b / (K*u[:,None]).sum(axis=0)
    P = np.diag(u) @ K @ np.diag(v)
    return P

def grad_OT(P, x, y):
    n = x.shape[0]
    d = x.shape[1]
    a = x.shape[2]
    m = y.shape[0]
    G = np.zeros((n+m,d,a))
    for k in range(n):
        tmp = np.zeros((d,a))
        for l in range(m):
            tmp += P[k,l]*(x[k,:]-y[l,:])
        G[k,:] = np.copy(tmp)
    for k in range(m):
        tmp = np.zeros((d,a))
        for l in range(n):
            tmp += P[k,l]*(y[k,:]-x[l,:])
        G[n+k,:] = np.copy(tmp)
    return G

def batch_sinkhorn_imputer(X_obs, binary_mask, batch_size=20, alpha=.6, eta=.8, epsilon=.06, n_iter=50):
    n, d, a = X_obs.shape
    a       = np.ones(batch_size)/batch_size
    b       = np.ones(batch_size)/batch_size
    imp     = np.zeros((2*batch_size, d, 1))
    # Gibbs Kernel computation
    K = lambda x2, y2, x, y : np.exp(- (np.tile(y2,(batch_size,1)) + np.tile(x2[:,np.newaxis],(1,batch_size)) - 2*x[:,0] @ y[:,0].T)/epsilon)
    for i in tqdm(range(n_iter)):
       x,y = sampler(X_obs, batch_size, n_batch=2)
       x2  = np.sum(x**2,1)[:,0]
       y2  = np.sum(y**2,1)[:,0]
       # Compute Entropic regularization of OT bw x and y
       P_ab = sinkhorn(K(x2, y2, x, y), a, b)
       # Compute AC of entropic regularization of OT in x
       P_aa = sinkhorn(K(x2, x2, x, x), a, a)
       # Compute AC of entropic regularization of OT in y
       P_bb = sinkhorn(K(y2, y2, y, y), b, b)
       # Compute Sinkhorn divergence gradient
       G    = grad_OT(P_ab, x, y) - .5*(grad_OT(P_aa, x, x) + grad_OT(P_bb, y, y))
       imp  -= alpha * G
    return imp

def main():
    X = sk_data.make_circles(100, noise=.01)[0]
    plotData(X.reshape(X.shape[0], 2, 1))
    X_all, X_obs, binary_mask = genData()
    imp = batch_sinkhorn_imputer(X_obs, binary_mask)
    X_los = np.copy(X_all)
    plot_mask = np.isfinite(X_los[:,0,0])*np.isfinite(X_los[:,1,0])
    X_los[np.ones(binary_mask.shape, dtype="int")-binary_mask] = nans(X_los[np.ones(binary_mask.shape, dtype="int")-binary_mask].shape)
    plt.figure()
    plt.plot(X_all[:,0,0], X_all[:,1,0], 'ro')
    plt.plot(imp[:,0,0], imp[:,1,0], 'bo')
    plt.show()
    plt.figure()
    plt.plot(X_los[:,0,0][plot_mask], X_los[:,1,0][plot_mask], 'ro')
    plt.show()

if __name__=="__main__":
    main()
